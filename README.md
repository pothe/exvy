# Ejemplo de Node.js con Express.js

**Aplicación Simple de Express.js**

# Funcionamiento

Esta app solo arranca un servidor en Express mediante Node.js.

Las siguientes direcciones están habilitadas:

'/gato'     - Es un pequeño ejemplo sobre un GET 

'nota.html' - Abre el archivo nota.html que viene en la carpeta 'public'

# Para usar en Docker
FROM [codenvy/node](https://hub.docker.com/r/codenvy/node/)

# ¿Cómo arrancar?

| #      | Descripción           | Comando         |
| :------|:----------------------| :---------------|
| 1      | Corre                 | `cd carpeta/app`|
| 2      | Corre                 | `node app.js`   |